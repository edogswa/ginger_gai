from app.libs.error import APIException


class Success(APIException):
    code = 201
    error_code = 0
    message = 'success'


class DeleteSuccess(Success):
    code = 202
    error_code = 1


class ClientTypeError(APIException):
    code = 400   # 请求参数错误。
    error_code = 1006
    message = 'client is invalid'


class ParameterException(APIException):
    code = 400
    message = 'invalid parameter'
    error_code = 1000


class ServerError(APIException):
    pass


class NotFound(APIException):
    code = 404
    message = 'not found'
    error_code = 1001


class AuthFailed(APIException):
    code = 401   # 授权失败
    error_code = 1005
    message = 'authorization failed 权限授权失败'


class Forbidden(APIException):
    code = 403    # 禁止访问
    error_code = 1004
    message = 'forbidden, not in scope'
