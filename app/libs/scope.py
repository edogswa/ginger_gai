class Scope:
    allow_api = []
    allow_module = []
    forbidden = []


class AdminScope(Scope):
    allow_api = ['v1.user.super_get_user']
    allow_module = ['v1.user']


class UserScope(Scope):
    # allow_api = ['v1.user.super_get_user']
    allow_module = ['v1.user']
    forbidden = ['v1.user.super_get_user']


def is_in_scope(scope, endpoint):
    scope = get_scopename(scope)
    scope = globals()[scope]()  # 从字符串变成类对象
    splits = endpoint.split('.')
    module_name = splits[0] + '.' + splits[1]
    if endpoint in scope.forbidden:
        return False
    if endpoint in scope.allow_api:
        return True
    if module_name in scope.allow_module:
        return True
    else:
        return False


def get_scopename(scope):
    scop_list = scope.split('_')
    scop_list = [scop.capitalize() for scop in scop_list]
    scope = ''.join(scop_list)
    return scope
