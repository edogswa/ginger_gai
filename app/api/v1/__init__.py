from flask import Blueprint
from app.api.v1 import book, user, client, token


def create_blueprint():
    bp_v1 = Blueprint('v1', __name__, url_prefix='/v1')

    bp_v1.register_blueprint(book.api)
    bp_v1.register_blueprint(user.api)
    bp_v1.register_blueprint(client.api)
    bp_v1.register_blueprint(token.api)
    return bp_v1

