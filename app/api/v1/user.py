from flask import Blueprint, jsonify, g

from app.libs.error_code import DeleteSuccess
from app.libs.token_auth import auth
from app.models.base import db
from app.models.user import User

api = Blueprint('user', __name__, url_prefix='/user')


@api.route('/<int:uid>', methods=['GET'])
@auth.login_required
def super_get_user(uid):
    user = User.query.filter_by(id=uid).first_or_404()
    return jsonify(user)


@api.route('/', methods=['GET'])
@auth.login_required
def get_user():
    uid = g.user.uid
    user = User.query.filter_by(id=uid).first_or_404()
    return jsonify(user)


# 管理员
@api.route('/<int:uid>', methods=['DELETE'])
@auth.login_required
def super_delete_user(uid):
    uid = g.user.uid
    with db.auto_commit():
        user = User.query.filter_by(id=uid).first_or_404(uid)
        user.delete()
    return DeleteSuccess()


@api.route('/', methods=['DELETE'])
@auth.login_required
def delete_user():
    uid = g.user.uid
    with db.auto_commit():
        user = User.query.filter_by(id=uid).first_or_404(uid)
        user.delete()
    return DeleteSuccess()


@api.route('/create')
def create_user():
    # API编程 发送的是数据
    # 客户端的种类很多，不仅仅是用户需要数据，第三方（服务器，APP，小程序）也可能需要数据
    # 注册形式很多，短信  邮件  QQ授权  微信授权
    return 'I am Edogswa'