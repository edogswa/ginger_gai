from flask import Blueprint, request
from app.validators.forms import ClientForm, UserEmailForm
from app.libs.enums import ClientTypeEnum
from app.models.user import User
from app.libs.error_code import ClientTypeError, Success

api = Blueprint('client', __name__, url_prefix='/client')


@api.route('/register', methods=['POST'])
def create_client():
    form = ClientForm().validate_for_api()
    promise = {
        ClientTypeEnum.USER_EMAIL : __register_user_by_email
    }
    # request.ages.to_dict()
    promise[form.type.data]()
    return Success()
    
    
def __register_user_by_email():
    form = UserEmailForm().validate_for_api()
    User.register_by_email(form.nickname.data, form.account.data, form.secret.data)